PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/sm8250-common

PRODUCT_COPY_FILES += \
    vendor/oneplus/apps/sm8250-common/proprietary/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    vendor/oneplus/apps/sm8250-common/proprietary/system/etc/sysconfig/hiddenapi-package-whitelist-oem.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist-oem.xml \
    vendor/oneplus/apps/sm8250-common/proprietary/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml \
    vendor/oneplus/apps/sm8250-common/proprietary/system_ext/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so

PRODUCT_PACKAGES += \
    CameraPicProcService \
    OnePlusCameraService \
    OnePlusCamera \
    OnePlusGallery
